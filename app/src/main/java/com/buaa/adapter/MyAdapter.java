package com.buaa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.buaa.activity.R;
import com.buaa.util.Globals;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/4/24.
 */
public class MyAdapter extends BaseAdapter {
    private Context context;
    private List<Map<String,Object>> values;
    public MyAdapter(Context context,List<Map<String ,Object>> values){
        this.context=context;
        this.values=values;
    }
    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.file_list,null);
            convertView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Globals.SCREEN_HIGHT/10));
        }
        TextView filetype_img= (TextView) convertView.findViewById(R.id.filetype_img);
        TextView file_fullpath= (TextView) convertView.findViewById(R.id.file_fullname);
        filetype_img.getLayoutParams().height=Globals.SCREEN_HIGHT/9;
        Map<String,Object> map=values.get(position);
        String extname=map.get("extname").toString();
        if((boolean)map.get("isfile")){
            if(Globals.filetypeImg.get(extname)==null){
                filetype_img.setBackgroundResource(R.drawable.file);
            }else{
                filetype_img.setBackgroundResource(Globals.filetypeImg.get(extname));
            }

        }else{
            filetype_img.setBackgroundResource(Globals.filetypeImg.get("close_dir"));
        }

        file_fullpath.setText(map.get("filename").toString());
        return convertView;
    }
}
